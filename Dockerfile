FROM node:6-alpine

COPY dist ./dist
COPY index.html ./
COPY server.js ./
COPY package*.json ./
RUN npm install

CMD ["node", "server.js"]
EXPOSE $PORT