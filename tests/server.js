var request = require("request");

var base_url = "http://localhost:8082/"

describe("Hello World Server", function() {
    describe("GET /hellowrold", function() {
        it("returns status code 200", function(done) {
            console.log("route", base_url + 'helloworld');
            request.get(base_url + 'helloworld', function(error, response, body) {
              expect(response.statusCode).toBe(200);
              done();
            });
          });
      
          it("returns Hello World", function(done) {
            request.get(base_url + 'helloworld', function(error, response, body) {
              expect(body).toBe("Hello World");
              done();
            });
          });
    });
});