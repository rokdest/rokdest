var express = require('express');
var app = express();
const PORT = process.env.PORT || 8082;

app.use("/helloworld", function(req, res) {
  res.send("Hello World!");
});

app.use('/', express.static(__dirname));

app.listen(PORT, function () {
  console.log('Example app listening on port ' + PORT + '!');
});